const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    app: ["./src/entry.js"]
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: 'bundle.js',
    publicPath: 'http://localhost:8988/dist/'
  },
  devServer: {
    contentBase: 'http://localhost:3000',
    compress: true,
    port: 8988,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader'
      }
    ]
  }
};

