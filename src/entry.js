const socket = io({
  transports: ['websocket']
}).connect('http://localhost:3000');

const form = document.getElementById("myForm");
const message = document.getElementById("message");

form.addEventListener('submit', e => {
  socket.emit('Ascii', message.value);
  message.value = '';
  e.preventDefault();
});

socket.on('Ascii', msg => {
  const node = document.createElement("li");
  const textnode = document.createTextNode(msg);

  node.appendChild(textnode);
  document.getElementById("messages").appendChild(node); 
});